<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

// $route['api/event'] = 'api/event/index';
// $route['api/event/store'] = 'api/event/store';
// $route['api/event/(:any)'] = 'api/event/show/$1';
// $route['api/event/(:any)/update'] = 'api/event/update/$1';
// $route['api/event/(:any)/delete'] = 'api/event/destroy/$1';

// $route['api/speaker']['get'] = 'api/speaker/index';
// $route['api/speaker']['post'] = 'api/speaker/store';
// $route['api/speaker/(:any)']['get'] = 'api/speaker/show/$1';
// $route['api/speaker/(:any)']['put'] = 'api/speaker/update/$1';
// $route['api/speaker/(:any)']['delete'] = 'api/speaker/destroy/$1';

// $route['api/event/(:any)/schedule']['get'] = 'api/schedule/index';
// $route['api/event/(:any)/schedule']['post'] = 'api/schedule/store';
// $route['api/event/(:any)/schedule/(:any)']['get'] = 'api/schedule/show/$1';
// $route['api/event/(:any)/schedule/(:any)']['put'] = 'api/schedule/update/$1';
// $route['api/event/(:any)/schedule/(:any)']['delete'] = 'api/schedule/destroy/$1';

$route['default_controller'] = 'welcome';
$route['404_override'] = 'error';
$route['translate_uri_dashes'] = false;
