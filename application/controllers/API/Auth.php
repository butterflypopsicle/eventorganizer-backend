<?php

use chriskacerguis\RestServer\RestController;

class Auth extends RestController
{
    public function login_post()
    {
        try {
            $email = $this->input->post('email');
            $password = $this->input->post('password');

            if (empty($email)) {
                throw new \Exception("Harap masukkan email anda.");
            }

            if (empty($email)) {
                throw new \Exception("Harap masukkan kata sandi anda.");
            }

            $this->db->from('user');
            $this->db->where('email', $email);
            $this->db->where('password', md5($password));
            $query = $this->db->get();
            $row = $query->row();

            if (!isset($row)) {
                throw new \Exception("Email / kata sandi salah.");
            }

            unset($row->password);

            $jwt = $this->jwt->encode([
                'sub' => (int) $row->id,
                'role' => $row->role,
                'email' => $row->email
            ]);

            return $this->response([
                'success' => true,
                'message' => null,
                'data' => $jwt
            ]);
        } catch (\Exception $e) {
            return $this->response([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ], 500);
        }
    }

    public function user_get()
    {
        try {
            $token = $this->user->token();
            $decodedToken = $this->jwt->decode($token);

            $this->db->where('id', $decodedToken->sub);
            $user = $this->db->get('user')->row();
            unset($user->password);

            if ($user->role == 'eo') {
                $user->data = $this->db
                    ->where('id_user', $user->id)
                    ->get('eo')
                    ->row();
            }

            if ($user->role == 'speaker') {
                $user->data = $this->db
                    ->where('id_user', $user->id)
                    ->get('pengisi_acara')
                    ->row();
            }

            if ($user->role == 'tenant') {
                $user->data = $this->db
                    ->where('id_user', $user->id)
                    ->get('tenant')
                    ->row();
            }

            return $this->response([
                'success' => true,
                'message' => null,
                'data' => $user
            ]);
        } catch (\Throwable $e) {
            return $this->response([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ], 500);
        }
    }

    public function user_put()
    {
        try {

            $token = $this->user->token();
            $decodedToken = $this->jwt->decode($token);

            $this->db->where('id', $decodedToken->sub);
            $user = $this->db->get('user')->row();
            $user_values = ['email' => $this->input->post('email')];

            if (!empty($this->input->post('old_password'))) {
                if ($user->password !== md5($this->input->post('old_password'))) {
                    throw new Exception('Kata sandi salah.');
                }

                if ($this->input->post('password') !== $this->input->post('password_confirm')) {
                    throw new Exception('Konfirmasi kata sandi tidak cocok.');
                }

                $user_values['password'] = md5($this->input->post('password'));
            }

            $this->db
                ->where('id', $user->id)
                ->update('user', $user_values);

            if ($user->role === 'eo') {
                $this->db->where('id_user', $user->id)
                    ->update('eo', $this->input->post('data'));
            }

            if ($user->role === 'speaker') {
                $this->db->where('id_user', $user->id)
                    ->update('pengisi_acara', $this->input->post('data'));
            }

            if ($user->role === 'tenant') {
                $this->db->where('id_user', $user->id)
                    ->update('tenant', $this->input->post('data'));
            }

            return $this->response([
                'success' => true,
                'message' => 'Profile berhasil di-update.',
                'data' => null
            ]);
        } catch (\Exception $e) {
            return $this->response([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ], 500);
        }
    }

    public function register_post()
    {
        try {
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $nama = $this->input->post('nama');
            $no_telp = $this->input->post('no_telp');
            $website = $this->input->post('website');
            $pemilik = $this->input->post('pemilik');
            $produk = $this->input->post('produk');
            $id_lembaga = $this->input->post('id_lembaga');
            $id_kategori = $this->input->post('id_kategori');
            $alamat = $this->input->post('alamat');
            $type = $this->input->post('type');

            // Validations.
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                throw new Exception('Invalid email address.');
            }

            // Check duplicates.
            $this->db->where('email', $email);
            $this->db->from('user');
            $duplicates = $this->db->count_all_results();
            if ($duplicates > 0) {
                throw new Exception('User dengan email tersebut sudah terdaftar.');
            }

            $this->db->trans_start();
            $this->db->insert('user', [
                'email' => $email,
                'password' => $password,
                'role' => $type,
                'active' => 1
            ]);

            $id_user = $this->db->insert_id();

            if ($type == 'eo') {
                $this->db->insert('eo', [
                    'id_user' => $id_user,
                    'nama' => $nama,
                    'no_telp' => $no_telp,
                    'website' => $website,
                    'id_lembaga' => $id_lembaga,
                    'alamat' => $alamat,
                ]);

                return $this->response([
                    'success' => true,
                    'message' => 'User successfully registered.',
                    'data' => null
                ], 200);
            }

            if ($type == 'tenant') {
                $this->db->insert('tenant', [
                    'id_user' => $id_user,
                    'nama' => $nama,
                    'no_telp' => $no_telp,
                    'website' => $website,
                    'pemilik' => $pemilik,
                    'produk' => $produk,
                    'alamat' => $alamat,
                ]);

                return $this->response([
                    'success' => true,
                    'message' => 'User successfully registered.',
                    'data' => null
                ], 200);
            }

            if ($type == 'speaker') {
                $this->db->insert('pengisi_acara', [
                    'id_user' => $id_user,
                    'nama' => $nama,
                    'no_telp' => $no_telp,
                    'website' => $website,
                    'id_kategori' => $id_kategori,
                    'alamat' => $alamat,
                ]);
            }

            $this->db->trans_complete();

            return $this->response([
                'success' => true,
                'message' => 'User successfully registered.',
                'data' => null
            ], 200);
        } catch (\Exception $e) {
            return $this->response([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ], 500);
        }
    }
}
