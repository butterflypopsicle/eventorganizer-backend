<?php

use chriskacerguis\RestServer\RestController;

class EO extends RestController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $id
     */
    public function index_get($id = null)
    {
        if ($id !== null) {
            return $this->show($id);
        }

        $limit = intval($this->input->get('limit') ?: 10);
        $page = intval($this->input->get('page') ?: 1);
        $status = $this->input->get('status');

        if ($status !== null) {
            $this->db->where('status', $status);
        }

        $this->db->limit($limit);
        $this->db->offset(($page - 1) * $limit);
        $data = $this->db->get('eo')->result_array();

        if ($status !== null) {
            $this->db->where('status', $status);
        }

        $this->db->limit($limit);
        $this->db->offset(($page - 1) * $limit);
        $this->db->from('eo');
        $total = $this->db->count_all_results();

        $id_lembaga = array_column($data, 'id_lembaga');
        $lembaga = $this->db->from('lembaga')
            ->where_in('id', $id_lembaga)
            ->get()
            ->result_array();

        // if ($this->db->error()) {
        //     return $this->response([
        //         'error' => $this->db->error()
        //     ]);
        // }

        foreach ($data as $i => $j) {
            $k = array_search($j['id_lembaga'], array_column($lembaga, 'id'));
            $data[$i]['lembaga'] = $lembaga[$k];
        }

        return $this->response([
            'success' => true,
            'data' => [
                'data' => $data,
                'current_page' => $page,
                'total' => $total
            ]
        ]);
    }

    /**
     * Show data details.
     * @param string $id ID of data.
     * @return mixed Event data.
     */
    public function show($id)
    {
        try {
            $this->db->where('id', $id);
            $data = $this->db->get('eo')->row();

            if (!$data) {
                throw new Exception('Event tidak ditemukan.');
            }

            return $this->response([
                'success' => true,
                'message' => null,
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return $this->response([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ], 500);
        }
    }

    /**
     * Store data.
     */
    public function index_post()
    {
        try {
            // $this->db->where('id', $id);
            // $data = $this->db->get('eo')->row();

            // if (!$data) {
            //     throw new Exception('Event tidak ditemukan.');
            // }

            return $this->response([
                'success' => true,
                'message' => null,
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return $this->response([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ], 500);
        }
    }
}
