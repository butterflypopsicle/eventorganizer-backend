<?php

use chriskacerguis\RestServer\RestController;

class Event extends RestController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $id
     */
    public function index_get($id = null)
    {
        if ($id !== null) {
            return $this->show($id);
        }

        $token = $this->user->token();
        $user = null;

        if ($token) {
            $decodedToken = $this->jwt->decode($token);
            $this->db->where('id', $decodedToken->sub);
            $user = $this->db->get('user')->row();

            if ($user->role == 'eo') {
                $user->data = $this->db
                    ->where('id_user', $user->id)
                    ->get('eo')
                    ->row();
            }

            if ($user->role == 'speaker') {
                $user->data = $this->db
                    ->where('id_user', $user->id)
                    ->get('pengisi_acara')
                    ->row();
            }

            if ($user->role == 'tenant') {
                $user->data = $this->db
                    ->where('id_user', $user->id)
                    ->get('tenant')
                    ->row();
            }
        }

        // if ($user !== null && $user->role === 'speaker') {
        //     $execute = $this->db->select('id_event')
        //         ->from('histori_pengisi_acara')
        //         ->where('id_pengisi_acara', $user->id)
        //         ->get();

        //     $event_ids = array_column($execute->result_array(), 'id');
        // }

        $status = $this->input->get('status');
        $search_by = $this->input->get('search_by');
        $search_query = $this->input->get('search_query');

        if ($status !== null) {
            $this->db->where('status', $status);
        }

        if (!empty($search_query) && !empty($search_by)) {
            $this->db->where($search_by . ' LIKE', '%' . $search_query . '%');
        }

        if ($user !== null) {
            $show_event = $this->input->get('show_event');
            if ($user->role === 'eo' && $show_event == 0) {
                $this->db->where('id_eo', $user->data->id);
            }

            if ($user->role === 'speaker') {
                $this->db->select('event.*');
                $this->db->join('histori_pengisi_acara', 'event.id = histori_pengisi_acara.id_event');
                $this->db->where('id_pengisi_acara', $user->data->id);
            }
        }

        $this->db->order_by('event.id', 'DESC');
        $execute = $this->db->get('event');

        // return $this->response([
        //     'wtf' => $this->db->error(),
        //     'ids' => $execute
        // ], 500);
        // return $this->response($this->db->error());

        $data = $execute->result();

        return $this->response([
            'success' => true,
            'data' => [
                'data' => $data,
                'total' => count($data)
            ]
        ]);
    }

    protected function show($id)
    {
        try {
            $this->db->where('id', $id);
            $data = $this->db->get('event')->row();
            $data->id_eo = intval($data->id_eo);

            $token = $this->user->token();
            $user = null;

            if ($token) {
                $decodedToken = $this->jwt->decode($token);
                $this->db->where('id', $decodedToken->sub);
                $user = $this->db->get('user')->row();

                if ($user->role == 'eo') {
                    $user->data = $this->db
                        ->where('id_user', $user->id)
                        ->get('eo')
                        ->row();
                }

                if ($user->role == 'speaker') {
                    $user->data = $this->db
                        ->where('id_user', $user->id)
                        ->get('pengisi_acara')
                        ->row();
                }

                if ($user->role == 'tenant') {
                    $user->data = $this->db
                        ->where('id_user', $user->id)
                        ->get('tenant')
                        ->row();
                }
            }

            if (!$data) {
                throw new Exception('Event tidak ditemukan.');
            }

            // Get EO
            $data->eo = $this->db->get_where('eo', ['id' => $data->id_eo])->row();

            // Get Joined elements.
            $data->joined_speakers = $this->db->get_where('histori_pengisi_acara', ['id_event' => $id])->result();
            $id_speakers = array_column($data->joined_speakers, 'id_pengisi_acara');
            $speakers = [];

            if (count($id_speakers) > 0) {
                $speakers = $this->db->where_in('id', $id_speakers)->get('pengisi_acara')->result();
                foreach ($data->joined_speakers as $ijs => $js) {
                    $foundIndex = array_search($js->id_pengisi_acara, array_column($speakers, 'id'));
                    $data->joined_speakers[$ijs]->speaker = $speakers[$foundIndex] ?: null;

                    $data->joined_speakers[$ijs]->id = intval($js->id);
                    $data->joined_speakers[$ijs]->id_pengisi_acara = intval($js->id_pengisi_acara);
                    $data->joined_speakers[$ijs]->harga_dari_eo = intval($js->harga_dari_eo);
                    $data->joined_speakers[$ijs]->harga_akhir = intval($js->harga_akhir);
                    $data->joined_speakers[$ijs]->harga_dari_pengisi_acara = intval($js->harga_dari_pengisi_acara);
                }
            }

            $data->joined_tenants = $this->db->get_where('histori_tenant', ['id_event' => $id])->result();
            $id_tenants = array_column($data->joined_tenants, 'id_tenant');
            $tenants = [];

            if (count($id_tenants) > 0) {
                $tenants = $this->db->where_in('id', $id_tenants)->get('tenant')->result();
                foreach ($data->joined_tenants as $ijt => $jt) {
                    $foundIndex = array_search($jt->id_tenant, array_column($tenants, 'id'));
                    $data->joined_tenants[$ijt]->tenant = $tenants[$foundIndex] ?: null;

                    $data->joined_tenants[$ijt]->id = intval($jt->id);
                    $data->joined_tenants[$ijt]->id_tenant = intval($jt->id_tenant);
                    $data->joined_tenants[$ijt]->harga_dari_eo = intval($jt->harga_dari_eo);
                    $data->joined_tenants[$ijt]->harga_dari_tenant = intval($jt->harga_dari_tenant);
                    $data->joined_tenants[$ijt]->harga_akhir = intval($jt->harga_akhir);

                    if ($user->role === 'tenant') {
                        $data->is_joined = $user->data->id == $jt->id_tenant;
                    }
                }
            }

            return $this->response([
                'success' => true,
                'message' => null,
                'data' => $data,
            ]);
        } catch (Exception $e) {
            return $this->response([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ], 500);
        }
    }

    /**
     * Store data.
     */
    public function index_post()
    {
        try {
            $token = $this->user->token();
            $decodedToken = $this->jwt->decode($token);

            $this->db->where('id', $decodedToken->sub);
            $user = $this->db->get('user')->row();

            $last_event = $this->db->from('event')
                ->order_by('id', 'DESC')
                ->limit(1)
                ->get()
                ->row();
            $id = intval(str_replace('PMR', '', $last_event->id));
            $data = $this->db->insert('event', [
                'id' => "PMR" . str_pad($id + 1, 5, "0", STR_PAD_LEFT),
                "kategori" => $this->input->post('kategori'),
                "keterangan" => $this->input->post('keterangan'),
                "nama" => $this->input->post('nama'),
                "status" => $this->input->post('status') ?: 'draft',
                "tanggal_akhir" => $this->input->post('tanggal_akhir'),
                "tanggal_mulai" => $this->input->post('tanggal_mulai'),
                'id_eo' => $user->role === 'eo' ? $user->id : null
            ]);

            if (!$data) {
                throw new Exception('Terjadi kesalahan. Mohon periksa log.');
            }

            return $this->response([
                'success' => true,
                'message' => null,
                'data' => $data
            ]);
        } catch (Exception $e) {
            return $this->response([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => $this->db->error()
            ], 500);
        }
    }

    /**
     * Update data.
     */
    public function index_put($id)
    {
        try {
            $data = $this->db->get_where('event', ['id' => $id])->row();

            if (!$data) {
                throw new Exception('Event tidak ditemukan.');
            }

            $this->db->where('id', $id);
            $update = $this->db->update('event', [
                "kategori" => $this->input->post('kategori') ?: $data->kategori,
                "keterangan" => $this->input->post('keterangan') ?: $data->keterangan,
                "nama" => $this->input->post('nama') ?: $data->nama,
                "status" => $this->input->post('status') ?: $data->status,
                "tanggal_akhir" => $this->input->post('tanggal_akhir') ?: $data->tanggal_akhir,
                "tanggal_mulai" => $this->input->post('tanggal_mulai') ?: $data->tanggal_mulai,
            ]);

            if (!$update) {
                throw new Exception('Terjadi kesalahan. Mohon periksa log.');
            }

            return $this->response([
                'success' => true,
                'message' => null,
                'data' => $data
            ]);
        } catch (Exception $e) {
            return $this->response([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => $this->db->error()
            ], 500);
        }
    }

    public function index_delete($id)
    {
        try {
            $this->db->from('event');
            $this->db->where('id', $id);
            $this->db->delete();

            return $this->response([
                'success' => true,
                'message' => 'Event berhasil dihapus.',
                'data' => null
            ]);
        } catch (Exception $e) {
            return $this->response([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ], 500);
        }
    }

    public function pending_get($id)
    {
        $token = $this->user->token();
        $decodedToken = $this->jwt->decode($token);

        $this->db->where('id', $decodedToken->sub);
        $user = $this->db->get('user')->row();

        if ($user->role == 'eo') {
            $user->data = $this->db
                ->where('id_user', $user->id)
                ->get('eo')
                ->row();
        }

        if ($user->role == 'speaker') {
            $user->data = $this->db
                ->where('id_user', $user->id)
                ->get('pengisi_acara')
                ->row();
        }

        if ($user->role == 'tenant') {
            $user->data = $this->db
                ->where('id_user', $user->id)
                ->get('tenant')
                ->row();
        }

        $this->db->from('histori_pengisi_acara');
        $this->db->where('id_event', $id);
        $this->db->where('id_pengisi_acara', $user->data->id);
        $execute = $this->db->get();
        $data = $execute->row_array();

        $data['konfirmasi_bukti_bayar'] = intval($data['konfirmasi_bukti_bayar']);

        return $this->response([
            'success' => true,
            'message' => 'Found',
            'data' => $data
        ]);
    }

    public function registration_get($id)
    {
        $token = $this->user->token();
        $decodedToken = $this->jwt->decode($token);

        $this->db->where('id', $decodedToken->sub);
        $user = $this->db->get('user')->row();

        if ($user->role == 'eo') {
            $user->data = $this->db
                ->where('id_user', $user->id)
                ->get('eo')
                ->row();
        }

        if ($user->role == 'speaker') {
            $user->data = $this->db
                ->where('id_user', $user->id)
                ->get('pengisi_acara')
                ->row();
        }

        if ($user->role == 'tenant') {
            $user->data = $this->db
                ->where('id_user', $user->id)
                ->get('tenant')
                ->row();
        }

        $this->db->from('histori_tenant');
        $this->db->where('id_event', $id);
        $this->db->where('id_tenant', $user->data->id);
        $execute = $this->db->get();
        $data = $execute->row_array();

        return $this->response([
            'success' => true,
            'message' => 'Found',
            'data' => $data
        ]);
    }
}
