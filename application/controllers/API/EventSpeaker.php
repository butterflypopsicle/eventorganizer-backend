<?php

use chriskacerguis\RestServer\RestController;

class EventSpeaker extends RestController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $id
     */
    public function index_get($id = null)
    {
        if ($id !== null) {
            return $this->show($id);
        }

        $token = $this->user->token();
        $user = null;

        if ($token) {
            $decodedToken = $this->jwt->decode($token);
            $this->db->where('id', $decodedToken->sub);
            $user = $this->db->get('user')->row();
        }

        $status = $this->input->get('status');
        $search_by = $this->input->get('search_by');
        $search_query = $this->input->get('search_query');

        if ($status !== null) {
            $this->db->where('status', $status);
        }

        if (!empty($search_query) && !empty($search_by)) {
            $this->db->where($search_by . ' LIKE', '%' . $search_query . '%');
        }

        if ($user !== null) {
            $show_event = $this->input->get('show_event');
            if ($user->role === 'eo' && $show_event == 0) {
                $this->db->where('id_eo', $user->id);
            }
        }

        $this->db->order_by('id', 'DESC');
        $data = $this->db->get('event')->result();

        return $this->response([
            'success' => true,
            'data' => [
                'data' => $data,
                'total' => count($data)
            ]
        ]);
    }

    protected function show($id)
    {
        try {
            $this->db->where('id', $id);
            $data = $this->db->get('event')->row();
            $data->id_eo = intval($data->id_eo);

            if (!$data) {
                throw new Exception('Event tidak ditemukan.');
            }

            // Get EO
            $data->eo = $this->db->get_where('eo', ['id' => $data->id_eo])->row();

            // Get Joined elements.
            $data->joined_speakers = $this->db->get_where('histori_pengisi_acara', ['id_event' => $id])->result();
            $id_speakers = array_column($data->joined_speakers, 'id_pengisi_acara');
            $speakers = [];

            if (count($id_speakers) > 0) {
                $speakers = $this->db->where_in('id', $id_speakers)->get('pengisi_acara')->result();
                foreach ($data->joined_speakers as $ijs => $js) {
                    $foundIndex = array_search($js->id_pengisi_acara, array_column($speakers, 'id'));
                    $data->joined_speakers[$ijs]->speaker = $speakers[$foundIndex] ?: null;

                    $data->joined_speakers[$ijs]->id = intval($js->id);
                    $data->joined_speakers[$ijs]->id_pengisi_acara = intval($js->id_pengisi_acara);
                    $data->joined_speakers[$ijs]->harga_dari_eo = intval($js->harga_dari_eo);
                    $data->joined_speakers[$ijs]->harga_akhir = intval($js->harga_akhir);
                    $data->joined_speakers[$ijs]->harga_dari_pengisi_acara = intval($js->harga_dari_pengisi_acara);
                }
            }

            $data->joined_tenants = $this->db->get_where('histori_tenant', ['id_event' => $id])->result();
            $id_tenants = array_column($data->joined_tenants, 'id_tenant');
            $tenants = [];

            if (count($id_tenants) > 0) {
                $tenants = $this->db->where_in('id', $id_tenants)->get('tenant')->result();
                foreach ($data->joined_tenants as $ijt => $jt) {
                    $foundIndex = array_search($jt->id_tenant, array_column($tenants, 'id'));
                    $data->joined_tenants[$ijt]->tenant = $tenants[$foundIndex] ?: null;

                    $data->joined_tenants[$ijt]->id = intval($jt->id);
                    $data->joined_tenants[$ijt]->id_tenant = intval($jt->id_tenant);
                    $data->joined_tenants[$ijt]->harga_dari_eo = intval($jt->harga_dari_eo);
                    $data->joined_tenants[$ijt]->harga_dari_tenant = intval($jt->harga_dari_tenant);
                    $data->joined_tenants[$ijt]->harga_akhir = intval($jt->harga_akhir);
                }
            }

            return $this->response([
                'success' => true,
                'message' => null,
                'data' => $data,
            ]);
        } catch (Exception $e) {
            return $this->response([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ], 500);
        }
    }

    /**
     * Store data.
     */
    public function index_post()
    {
        try {
            $data = $this->db->insert('histori_pengisi_acara', [
                'status' => 'pending',
                'id_event' => $this->input->post('id_event'),
                'keterangan' => $this->input->post('keterangan'),
                'id_pengisi_acara' => $this->input->post('id_pengisi_acara'),
                'harga_dari_eo' => $this->input->post('harga_dari_eo', 0)
            ]);

            if (!$data) {
                throw new Exception('Terjadi kesalahan. Mohon periksa log.');
            }

            return $this->response([
                'success' => true,
                'message' => null,
                'data' => $data
            ]);
        } catch (Exception $e) {
            return $this->response([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => $this->db->error()
            ], 500);
        }
    }

    /**
     * Update data.
     */
    public function index_put($id)
    {
        try {
            $data = $this->db->get_where('histori_pengisi_acara', ['id' => $id])->row();

            if (!$data) {
                throw new Exception('Event tidak ditemukan.');
            }

            $this->db->where('id', $id);
            $update = $this->db->update('histori_pengisi_acara', [
                'status' => $this->input->post('status') ?? $data->status,
                'id_event' => $this->input->post('id_event') ?? $data->id_event,
                'keterangan' => $this->input->post('keterangan') ?? $data->keterangan,
                'id_pengisi_acara' => $this->input->post('id_pengisi_acara') ?? $data->id_pengisi_acara,
                'harga_dari_eo' => $this->input->post('harga_dari_eo', 0) ?? $data->harga_dari_eo,
                'bukti_bayar' => $this->input->post('bukti_bayar') ?? $data->bukti_bayar,
                'konfirmasi_bukti_bayar' => $this->input->post('konfirmasi_bukti_bayar') ?? $data->konfirmasi_bukti_bayar,
                'tanggal_konfirmasi_bukti_bayar' => $data->tanggal_konfirmasi_bukti_bayar ?? date('Y-m-d H:i:s'),
            ]);

            if (!$update) {
                throw new Exception('Terjadi kesalahan. Mohon periksa log.');
            }

            return $this->response([
                'success' => true,
                'message' => null,
                'data' => $data
            ]);
        } catch (Exception $e) {
            return $this->response([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => $this->db->error()
            ], 500);
        }
    }

    public function index_delete($id)
    {
        try {
            $this->db->from('histori_pengisi_acara');
            $this->db->where('id', $id);
            $this->db->delete();

            return $this->response([
                'success' => true,
                'message' => 'Event berhasil dihapus.',
                'data' => null
            ]);
        } catch (Exception $e) {
            return $this->response([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ], 500);
        }
    }

    public function upload_post($id)
    {
        try {
            $config['upload_path']          = './upload/speaker/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['file_name']            = 'speaker' . $id . '_' . date('YmdHis');

            $this->load->library('upload', $config);

            $file_name = '';
            if (!$this->upload->do_upload('file')) {
                $error = array('error' => $this->upload->display_errors());

                return $this->response([
                    'success' => false,
                    'message' => 'Event gagal di-update.',
                    'data' => $error
                ], 500);
            }

            $upload_data = $this->upload->data();

            $this->db
                ->where(['id' => $id])
                ->update('histori_pengisi_acara', [
                    'bukti_bayar' => base_url('upload/speaker/' . $upload_data['file_name'])
                ]);

            return $this->response([
                'success' => true,
                'message' => 'Event berhasil di-update.',
                'data' => null
            ]);
        } catch (Exception $e) {
            return $this->response([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ], 500);
        }
    }
}
