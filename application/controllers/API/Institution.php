<?php

use chriskacerguis\RestServer\RestController;

class Institution extends RestController
{

    public function index_get()
    {
        $data = $this->db->get('lembaga')->result();

        return $this->response([
            'success' => true,
            'data' => $data
        ]);
    }
}
