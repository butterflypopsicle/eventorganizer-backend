<?php

use chriskacerguis\RestServer\RestController;

class Schedule extends RestController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $id
     */
    public function index_get($id = null)
    {
        if ($id !== null) {
            return $this->show($id);
        }

        $limit = intval($this->input->get('limit') ?: 10);
        $page = intval($this->input->get('page') ?: 1);
        $status = $this->input->get('status');

        if ($status !== null) {
            $this->db->where('status', $status);
        }

        $this->db->where('id_event', $id);
        $this->db->limit($limit);
        $this->db->offset(($page - 1) * $limit);
        $data = $this->db->get('jadwal_event')->result();

        if ($status !== null) {
            $this->db->where('status', $status);
        }

        $this->db->limit($limit);
        $this->db->offset(($page - 1) * $limit);
        $this->db->from('jadwal_event');
        $total = $this->db->count_all_results();

        return $this->response([
            'success' => true,
            'data' => [
                'data' => $data,
                'current_page' => $page,
                'total' => $total
            ]
        ]);
    }

    /**
     * Show data details.
     * @param string $id ID of data.
     * @return mixed Schedule data.
     */
    public function show($id)
    {
        try {
            $this->db->where('id', $id);
            $data = $this->db->get('jadwal_event')->row();

            if (!$data) {
                throw new Exception('Jadwal tidak ditemukan.');
            }

            return $this->response([
                'success' => true,
                'message' => null,
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return $this->response([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ], 500);
        }
    }

    /**
     * Store data.
     */
    public function index_post($id)
    {
        try {
            // $this->db->where('id', $id);
            // $data = $this->db->get('jadwal_event')->row();

            // if (!$data) {
            //     throw new Exception('Jadwal jadwal_event tidak ditemukan.');
            // }

            return $this->response([
                'success' => true,
                'message' => null,
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return $this->response([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ], 500);
        }
    }
}
