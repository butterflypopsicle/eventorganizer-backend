<?php

use chriskacerguis\RestServer\RestController;

class Speaker extends RestController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $id
     */
    public function index_get($id = null)
    {
        if ($id !== null) {
            return $this->show($id);
        }

        $limit = intval($this->input->get('limit') ?: 10);
        $page = intval($this->input->get('page') ?: 1);
        $status = $this->input->get('status');

        if ($status !== null) {
            $this->db->where('status', $status);
        }

        $this->db->limit($limit);
        $this->db->offset(($page - 1) * $limit);
        $data = $this->db->get('pengisi_acara')->result();

        if ($status !== null) {
            $this->db->where('status', $status);
        }

        $this->db->limit($limit);
        $this->db->offset(($page - 1) * $limit);
        $this->db->from('pengisi_acara');
        $total = $this->db->count_all_results();

        return $this->response([
            'success' => true,
            'data' => [
                'data' => $data,
                'current_page' => $page,
                'total' => $total
            ]
        ]);
    }

    /**
     * Show data details.
     * @param string $id ID of data.
     * @return mixed Event data.
     */
    public function show($id)
    {
        try {
            $this->db->where('id', $id);
            $data = $this->db->get('pengisi_acara')->row();

            if (!$data) {
                throw new Exception('Event tidak ditemukan.');
            }

            return $this->response([
                'success' => true,
                'message' => null,
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return $this->response([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ], 500);
        }
    }

    /**
     * Store data.
     */
    public function index_post()
    {
        try {
            $this->db->trans_start();

            // Create user.
            $this->db->insert('user', [
                'email' => $this->input->post('email'),
                'password' => md5($this->input->post('password')),
                'role' => 'speaker',
                'active' => 1
            ]);

            $id_user = $this->db->insert_id();

            // Create Speaker.
            $this->db->insert('pengisi_acara', [
                'nama' => $this->input->post('nama'),
                'id_kategori' => $this->input->post('id_kategori'),
                'alamat' => $this->input->post('alamat'),
                'no_telp' => $this->input->post('no_telp'),
                'website' => $this->input->post('website'),
                'minimum_tawaran' => $this->input->post('minimum_tawaran'),
                'id_user' => $id_user,
            ]);

            $this->db->trans_complete();

            return $this->response([
                'success' => true,
                'message' => null,
                'data' => $id_user
            ]);
        } catch (\Exception $e) {
            return $this->response([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => $this->db->error()
            ], 500);
        }
    }

    /**
     * Store data.
     */
    public function index_put($id)
    {
        try {
            $this->db->trans_start();

            $speaker = $this->db->get_where('pengisi_acara', ['id' => $id])->first();
            $user = $this->db->get_where('user', ['id' => $speaker->id_user])->first();

            // Update user.
            if (
                $this->input->post('email') !== $user->email ||
                $this->input->post('password') !== null
            ) {
                $this->db->where('id', $speaker->id_user)
                    ->update('user', [
                        'email' => $this->input->post('email'),
                        'password' => md5($this->input->post('password')),
                        'role' => 'speaker',
                        'active' => 1
                    ]);
            }

            // Create Speaker.
            $this->db->where('id', $id)
                ->update('speaker', [
                    'nama' => $this->input->post('nama'),
                    'id_kategori' => $this->input->post('id_kategori'),
                    'alamat' => $this->input->post('alamat'),
                    'no_telp' => $this->input->post('no_telp'),
                    'website' => $this->input->post('website'),
                    'minimum_tawaran' => $this->input->post('minimum_tawaran'),
                ]);

            $this->db->trans_complete();

            if (!$this->db->trans_status()) {
                throw new Exception('Terjadi kesalahan. Mohon periksa log.');
            }

            return $this->response([
                'success' => true,
                'message' => null,
                'data' => null
            ]);
        } catch (\Exception $e) {
            return $this->response([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => $this->db->error()
            ], 500);
        }
    }
}
