<?php

use chriskacerguis\RestServer\RestController;

class SpeakerCategory extends RestController
{
    public function index_get($id = null)
    {
        if ($id !== null) {
            return $this->show($id);
        }

        $data = $this->db->get('kategori_pengisi_acara')->result();

        return $this->response([
            'success' => true,
            'message' => null,
            'data' => $data
        ]);
    }

    /**
     * Show data details.
     * @param string $id ID of data.
     * @return mixed Kategori data.
     */
    public function show($id)
    {
        try {
            $this->db->where('id', $id);
            $data = $this->db->get('kategori_pengisi_acara')->row();

            if (!$data) {
                throw new Exception('Kategori tidak ditemukan.');
            }

            return $this->response([
                'success' => true,
                'message' => null,
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return $this->response([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ], 500);
        }
    }

    /**
     * Update data.
     */
    public function index_post()
    {
        try {
            $data = $this->db->insert('kategori_pengisi_acara', [
                'nama' => $this->input->post('nama')
            ]);

            return $this->response([
                'success' => true,
                'message' => null,
            ]);
        } catch (Exception $e) {
            return $this->response([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ], 500);
        }
    }

    /**
     * Update data.
     */
    public function index_put($id)
    {
        try {
            $data = $this->db->get_where('kategori_pengisi_acara', ['id' => $id])->row();

            if (!$data) {
                throw new Exception('Kategori tidak ditemukan.');
            }

            $this->db->where('id', $id);
            $this->db->update('kategori_pengisi_acara', [
                'nama' => $this->input->post('nama')
            ]);

            return $this->response([
                'success' => true,
                'message' => null,
                'data' => $data
            ]);
        } catch (Exception $e) {
            return $this->response([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ], 500);
        }
    }

    public function index_delete($id)
    {
        try {
            $this->db->from('kategori_pengisi_acara');
            $this->db->where('id', $id);
            $this->db->delete();

            return $this->response([
                'success' => true,
                'message' => 'Kategori berhasil dihapus.',
                'data' => null
            ]);
        } catch (Exception $e) {
            return $this->response([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ], 500);
        }
    }
}
