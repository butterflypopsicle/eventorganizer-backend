<?php

class Error extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: Origin, Authorization, X-Requested-With, Content-Type, Accept");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

        if (strpos($this->input->get_request_header('Content-Type'), 'application/json') !== -1) {
        }

        return $this->response([
            'success' => false,
            'message' => 'Route not found.',
            'data' => null
        ], 404);
    }
}
