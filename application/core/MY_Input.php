<?php

class MY_Input extends CI_Input
{
    protected $json = [];

    public function __construct()
    {
        parent::__construct();

        if (strpos($this->get_request_header('Content-Type'), 'application/json') !== -1) {
            $this->json = json_decode(file_get_contents('php://input', true));
        }
    }

    public function post($index = null, $xss_clean = null)
    {
        if (strpos($this->get_request_header('Content-Type'), 'application/json') !== -1) {
            $index = str_replace('.', '->', $index);
            return isset($this->json->{$index}) ? $this->json->{$index} : null;
        }

        return parent::post($index, $xss_clean);
    }
}
