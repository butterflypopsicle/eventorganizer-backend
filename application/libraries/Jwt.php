<?php
class Jwt
{
    public static function encode($payload)
    {
        // Create token header as a JSON string
        $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);

        // Create token payload as a JSON string
        $payload = json_encode($payload);

        // Encode Header to Base64Url String
        $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));

        // Encode Payload to Base64Url String
        $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

        // Create Signature Hash
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, 'secret', true);

        // Encode Signature to Base64Url String
        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

        // Create JWT
        $jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;

        return $jwt;
    }

    public static function decode($jwt)
    {
        $jwt = explode(".", $jwt);
        $payload = base64_decode(str_replace(['-', '_', ''], ['+', '/', '='],  $jwt[1]));
        $sign = hash_hmac('sha256', $jwt[0] . "." . $jwt[1], 'secret', true);
        $checkSign = base64_decode(str_replace(['-', '_', ''], ['+', '/', '='],  $jwt[2]));
        if ($sign !== $checkSign) {
            throw new Exception('Invalid payload.');
        }
        return json_decode($payload);
    }
}
