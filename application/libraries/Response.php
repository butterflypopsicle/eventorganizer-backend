<?php

class Response
{
    public function json($array, $code = 200)
    {
        $CI = &get_instance();
        $CI->output->set_content_type('application/json');
        $CI->output->set_status_header($code ?: 200);
        $CI->output->set_output(json_encode($array));
    }
}
