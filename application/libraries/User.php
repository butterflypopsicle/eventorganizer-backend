<?php

class User
{
    protected $CI = null;

    public function __construct()
    {
        $this->CI = &get_instance();
    }

    public function token()
    {
        $bearer = $this->CI->input->get_request_header('Authorization');
        $token = str_replace('Bearer ', '', $bearer);

        return $token;
    }
}
